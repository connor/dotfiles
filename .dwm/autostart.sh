# restore nitrogen

nitrogen --restore &

(conky | while read LINE; do xsetroot -name "$LINE"; done) &

xmodmap -e "keycode 108 = Super_L" & # reassign Alt_R to Super_L
xmodmap -e "remove mod1 = Super_L" & # make sure X keeps it out of the mod1 group

setxkbmap -option keypad:pointerkeys &

setxkbmap -option caps:escape &

# make the monitors seperate

#xrandr --output VGA1 --auto --right-of LVDS1 &

# run xrdb on startup
xrdb ~/.Xresources

# startup the clipmenu daemon
clipmenud

#run acompositor

xcompmgr -c -r 0 &

# run slock

xautolock -time 10 -locker slock &

# haha cursor go zoom
#xset r 300 50 &



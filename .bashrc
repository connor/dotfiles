#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\[\033[95m\]\u\[\033[90m\]@\[\033[94m\]\h \w\[\033[37m\]] $ '

alias oialt="mpv 'Talking Heads - Once in a Lifetime (Official Video)-5IsSpAOD6K8.mkv'"
alias rickroll="mpv ~/Videos/'Rick Astley - Never Gonna Give You Up (Video)-dQw4w9WgXcQ.mkv'"
alias sepmultimonitor='xrandr --output VGA1 --auto --right-of LVDS1'

alias cpfetch="clear && pfetch"
alias sl="sl | lolcat"

if [ -f /etc/bash.command-not-found ]; then
	    . /etc/bash.command-not-found
fi

# plan9 stuff

PLAN9=/usr/local/plan9 export PLAN9
PATH=$PATH:$PLAN9/bin export PATH

# dotfiles git repo alias

alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

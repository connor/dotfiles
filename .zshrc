# welcome to Connor's Zshrc! Most of this I riped out of Luke Smith's (https://lukesmith.xyz) Zshrc file.
HISTFILE=~/.zshhistory
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/connormagnusson/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

autoload -U colors && colors

# this is my custom prompt. See te archwiki on how to make your own.
PS1="%B%{$fg[white]%}[%{$fg[magenta]%}%n%{$fg[grey]%}@%{$fg[blue]%}%M %~ %{$fg[white]%}]%# %b"
RPS1="%B%{$fg[white]%}%t%b"

setopt autocd # if you put in a directory name without cd, it will just automatically cd into it.

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# aliases

alias sl="sl | lolcat"
alias fl="fortune | lolcat"
alias cpfetch="clear && pfetch"
alias sepmultimonitor='xrandr --output VGA1 --auto --right-of LVDS1'
alias Xr="vim ~/.Xresources"
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias dave_gnukem="dave_gnukem -f"
alias ls="ls --color=auto"
alias grep="grep --color=auto"

alias -g ...='../..'
alias -g ....='../../..'

# sum plan9 stuff
PLAN9=/usr/local/plan9 export PLAN9
PATH=$PATH:$PLAN9/bin export PATH
